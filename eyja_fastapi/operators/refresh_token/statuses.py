class RefreshTokenStatuses:
    ACTIVE = 'active'
    REVOKED = 'revoked'
    CANCELED = 'canceled'
