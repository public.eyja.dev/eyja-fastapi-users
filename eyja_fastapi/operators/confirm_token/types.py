class ConfirmTokenTypes:
    AUTH = 'auth'
    RESET = 'reset'
    UNKNOWN = 'unknown'
